# dotfiles

## Requirements

Execute [the boot script](/data/boot.bash)

## Install

Clone (as `~/.dotfiles`) and Install the dotfiles:

    env RCRC=$HOME/.dotfiles/rcrc rcup
    cp ~/.dotfiles/config/home-manager/flake.nix ~/.config/home-manager/
    cd ~/.config/home-manager/
    nix flake update
    nix run home-manager/master -- init --switch --impure

After the initial installation, you can run `rcup` without the one-time variable
`RCRC` being set (`rcup` will symlink the repo’s `rcrc` to `~/.rcrc` for future
runs of `rcup`). [See
example](https://github.com/thoughtbot/dotfiles/blob/master/rcrc).

This command will create symlinks for config files in your home directory.
Setting the `RCRC` environment variable tells `rcup` to use standard
configuration options.

## Update

From time to time you should pull down any updates to these dotfiles, and run

    rcup

to link any new files and install new vim plugins. **Note** You _must_ run
`rcup` after pulling to ensure that all files in plugins are properly installed,
but you can safely run `rcup` multiple times so update early and update often!

## License

dotfiles is copyright © 2009-2018 thoughtbot. It is free software, and may be
redistributed under the terms specified in the [`LICENSE`] file.

[`license`]: /LICENSE

## Develop

    nix flake init --template github:cachix/devenv
    nix develop --impure -c $SHELL
