with (import <nixpkgs> { });
let
  rstudio = pkgs.rstudioWrapper.override
    {
      packages = with pkgs.rPackages; [ tidyverse ];
    };
in
pkgs.mkShell
{
  buildInputs = [ rstudio ];
  shellHook = ''
    export LANG="C.utf8"
  '';
}

