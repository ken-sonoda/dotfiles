#!/bin/bash -eu

dir=$(mktemp -d)
trap "rm -fr ${dir}" EXIT
sudo echo building
git clone --branch 0.1.7 --depth 1 https://github.com/wachikun/yaskkserv2.git $dir
(
    cd $dir
    nix-shell --pure -p cargo pkg-config openssl \
        -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/9957cd48326fe8dbd52fdc50dd2502307f188b0d.tar.gz \
        --command "cargo build --release"

    sudo cp -av target/release/yaskkserv2 /usr/local/sbin/
    sudo cp -av target/release/yaskkserv2_make_dictionary /usr/local/bin/
)

install_path=$HOME/.config/yaskkserv
mkdir -p $install_path
yaskkserv2_make_dictionary \
    --dictionary-filename=$install_path/dictionary.yaskkserv2 \
    /usr/share/skk/SKK-JISYO.L

! grep yaskkserv2 $HOME/.profile && echo '
if command -v yaskkserv2 &> /dev/null
then
yaskkserv2 \
    --google-cache-filename=/tmp/yaskkserv2.cache \
    $HOME/.config/yaskkserv/dictionary.yaskkserv2
fi
' >> $HOME/.profile
! grep type=server ~/.local/share/fcitx5/skk/dictionary_list && fcitx5-configtool

