{ config, pkgs, lib, ... }: {
  programs.tmux = {
    enable = true;
    sensibleOnTop = true;
    terminal = "screen-256color";
    baseIndex = 1; # match keyboard order with tmux window order
    shell = "${pkgs.zsh}/bin/zsh";
    reverseSplit = true;
    keyMode = "vi";
    customPaneNavigationAndResize = true;
    resizeAmount = 10;
    prefix = "C-s";
    disableConfirmationPrompt = true;
    aggressiveResize = true;
    clock24 = true;
    escapeTime = 0;
    historyLimit = 50000;
    plugins = with pkgs.tmuxPlugins;[
      {
        plugin = continuum;
        extraConfig = "set -g @continuum-restore 'on'";
      }
      {
        plugin = resurrect;
        extraConfig = ''
          set -g @resurrect-capture-pane-contents 'on'
          set -g @resurrect-strategy-nvim 'session'
        '';
      }
      copycat
      open
      sessionist
      yank
    ];

    extraConfig = ''
      set-option -g status-position top
      set -g renumber-windows on

      set-option -g status-right '#[fg=black,bright][ #[fg=white,nobright]#(uptime | rev | cut -d":" -f1 | rev | cut -d"," -f1)#[fg=black,bright] ] %a %h-%d %H:%M'

      unbind-key C-z # don't suspend-client

      set-option -g mouse on

      bind-key -T copy-mode-vi C-WheelUpPane send-keys -X halfpage-up
      bind-key -T copy-mode-vi C-WheelDownPane send-keys -X halfpage-down

    '' + lib.readFile "${./f-keys.tmux}" + ''
      # Local config
      if-shell "[ -f ~/.tmux.conf.local ]" 'source ~/.tmux.conf.local'
    '';
  };
}
