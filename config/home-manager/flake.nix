{
  description = "Home Manager configuration of vega";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixgl = {
      url = "github:nix-community/nixGL";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, nixgl, ... }:
    let
      pkgs = import nixpkgs { };
      username = builtins.getEnv "USER"; # with impure

    in
    {
      homeConfigurations.${username} = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        extraSpecialArgs = {
          inherit nixgl;
        };

        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [
          {
            home = {
              inherit username;
              homeDirectory = "/home/${username}";
            };
          }
          ./home.nix
          ./shell_prg.nix
          ./multiplexer.nix
          ./runtime.nix
          ./tool.nix
          ./editor/default.nix
          ./terminal.nix
        ];

        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };
    };
}
