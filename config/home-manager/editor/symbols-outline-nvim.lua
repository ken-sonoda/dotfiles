require("symbols-outline").setup({
	auto_close = true,
	show_relative_numbers = true,
	auto_preview = false,
})

vim.api.nvim_set_keymap('n', '<C-b>', ':SymbolsOutline<CR>', {})
