require("treesitter-context").setup({
	enable = true,
	throttle = true,
	max_lines = 0, -- How many lines the window should span. Values <= 0 mean no limit.
	patterns = {
		-- Match patterns for TS nodes. These get wrapped to match at word boundaries.
		default = {
			"class",
			"function",
			"method",
			"for",
			"while",
			"if",
			"switch",
			"case",
		},
		--   rust = {
		--       'impl_item',
		--   },
	},
	exact_patterns = {
		-- Treat patterns.rust as a Lua pattern
		-- rust = true,
	},
})
