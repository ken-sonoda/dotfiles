{ pkgs, config, nixgl, ... }:
{
  nixGL = {
    packages = nixgl.packages; # you must set this or everything will be a noop
  };

  home.packages = [
  ];

  programs.wezterm = {
    enable = true;
    # https://github.com/nix-community/nixGL/issues/114#issuecomment-2438420340
    package = config.lib.nixGL.wrap pkgs.wezterm;
    extraConfig = ''
      return {
        front_end = "WebGpu",
        font_size = 18.0,
        default_prog = {
          "${pkgs.tmux}/bin/tmux"
        },
        window_background_opacity = 0.9,
        use_ime = true,
        ime_preedit_rendering = "Builtin"
      }
    '';
    colorSchemes = { };
  };
}
